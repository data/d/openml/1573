# OpenML dataset: gisette

https://www.openml.org/d/1573

**WARNING: This dataset is archived. Use [meta](https://gitlab.com/data/meta) to discuss it.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Isabelle Guyon","Steve Gunn","Asa Ben Hur","and Gideon Dror. NIPS  
libSVM","AAD group  
**Source**: [original](http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary.html) - Date unknown  
**Please cite**:   

#Dataset from the LIBSVM data repository.

Preprocessing: The data set is also available at UCI. Because the labels of testing set are not available, here we use the validation set (gisette_valid.data and gisette_valid.labels) as the testing set. The training data (gisette_train) are feature-wisely scaled to [-1,1]. Then the testing data (gisette_valid) are scaled based on the same scaling factors for the training data.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1573) of an [OpenML dataset](https://www.openml.org/d/1573). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1573/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1573/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1573/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

